
import torch
# noinspection PyProtectedMember
from torch.nn.modules.loss import _Loss
import torch.nn.functional as F

class TVLoss(_Loss):
    def __init__(self):
        super(TVLoss, self).__init__()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError


class AsymmetricLoss(_Loss):
    def __init__(self):
        super(AsymmetricLoss, self).__init__()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError

class TotalLoss(_Loss):
    def __init__(self, lambda_tv, lambda_asymm):
        super(TotalLoss, self).__init__()
        self.lambda_tv = lambda_tv
        self.lambda_asymm = lambda_asymm
        self.loss = torch.nn.MSELoss()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        return self.loss(inp, target)

class FocalLoss(_Loss):
    def __init__(self, gamma=2, alpha=None, reduction='mean'):
        super(FocalLoss, self).__init__()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = reduction

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        # Compute Cross Entropy Loss
        ce_loss = F.cross_entropy(inp, target, reduction='none')

        # Compute Focal Loss
        pt = torch.exp(-ce_loss)
        focal_loss = (1 - pt) ** self.gamma * ce_loss

        # Apply class weights if alpha is provided
        if self.alpha is not None:
            focal_loss = self.alpha[target] * focal_loss
        
        return focal_loss.mean()