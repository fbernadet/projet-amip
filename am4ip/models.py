
import torch
from torch.nn import Module


class CBDNetwork(Module):
    def __init__(self, encoder: Module,
                 decoder: Module):
        super(CBDNetwork, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, x: torch.Tensor):
        """Forward pass for the network.

        :return: A torch.Tensor.
        """
        return self.decoder(x)
