# Projet AMIP

## Instructions to train a model

This Jupyter notebook contains the code for making predictions on an image using a trained model.

1. Load all the dependencies from the `imports` cell.
2. Load all the data by running the `Load data` cell.
3. In the `Create model` cell, load the `Block` class and then load the model you want to use.
4. In the `Define Loss` cell, load the loss function you want to use.
5. Then in the `Compile and train model` cell, compile the selected model and train it. After this you can save and load the model.
6. Finally, you can load all the cells from the `Evaluate model` cell to evaluate the model on the test set using multiple metrics.
7. The last cell `Show Example` can be used to show some examples of the model's predictions.

## Instructions to load a model

1. Load all the dependencies from the `imports` cell.
2. Load a random model under the `Compile and train model` cell.
3. Load the model of your choice from the "model/" directory by changing the path under the `Load model` cell. (the best is `unet_30epochsWeightedEntropy.pt`)
4. Run it.
